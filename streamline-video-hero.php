<?php
/*
Plugin Name: Streamline Video Hero
Plugin URI: http://www.streamlinevrs.com
Description: Embed Video as Hero Image on Streamline themes
Version: 1.0.0
Author: Tom Ransom
Author URI: http://1bigidea.com
Network Only: false

Licensed under The MIT License (MIT)

Copyright 2016 Streamline Vacation Rental Software

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

require 'plugin_update_check.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
   'https://kernl.us/api/v1/updates/570c71dee54521197b44631e/',
   __FILE__,
   'my-plugin-slug',
   1
);

class Streamline_Video_Embed {
	private static $_this;
	var $plugin_slug = "Streamline_Video_Embed";
	var $class_prefix = 'Streamline_Video_';
	var $plugin_name = "Streamline Video Hero";
	var $plugin_version = "1.0";
	var $setting_prefix = 'streamline_theme_';

	private function __construct() {

		register_activation_hook(   __FILE__, array( __CLASS__, 'activate'   ) );
		register_deactivation_hook( __FILE__, array( __CLASS__, 'deactivate' ) );

		add_action('init', array($this, 'init'));

		spl_autoload_register( array($this, 'class_file_autoloader') );

	}
	function __destruct(){
        // flush_rewrite_rules();
	}
	public static function activate() {
		// Add options, initiate cron jobs here

        // flush_rewrite_rules();
	}
	function deactivate() {
		// Remove cron jobs here
	}
	function uninstall() {
		// Delete options here
	}
	static function get_instance(){
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// http://7php.com/how-to-code-a-singleton-design-pattern-in-php-5/
		if( !is_object(self::$_this) ) self::$_this = new self();

		return self::$_this;
	}

	/**
	 * Autoloads files when requested
	 *
	 * @since  1.0.0
	 * @param  string $class_name Name of the class being requested
	 * http://dsgnwrks.pro/how-to/using-class-autoloaders-in-wordpress/
	 */
	function class_file_autoloader( $class_name ) {

		/**
		 * If the class being requested does not start with our prefix,
		 * we know it's not one in our project
		 */
		if ( 0 !== strpos( $class_name, $this->class_prefix ) ) {
			return;
		}

		$file_name = str_replace(
			array( $this->class_prefix, '_' ),      // Prefix | Underscores
			array( '', '-' ),         // Remove | Replace with hyphens
			strtolower( $class_name ) // lowercase
		);

		// Compile our path from the current location
		$file = dirname( __FILE__ ) . '/includes/class-'. $file_name .'.php';

		// If a file is found
		if ( file_exists( $file ) ) {
			// Then load it up!
			require( $file );
		}
	}

	/**
	 *	Functions below actually do the work
	 */
	function init(){
		add_action( 'streamline_hero', array( $this, 'insert_video') );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_stuff') );
	}

	function enqueue_stuff(){

		wp_enqueue_script( 'streamline-hero', plugins_url( '/assets/js/streamline-video-hero.js', __FILE__), array('jquery') );
		wp_enqueue_style ( 'streamline-hero', plugins_url( '/assets/css/streamline-video-hero.css', __FILE__) );
	}

	function insert_video(){

		$poster = get_theme_mod( $this->setting_prefix . 'video_hero_poster', 0 );
		$poster_url = '';
		if( $poster ) $poster_url = wp_get_attachment_url( $poster );

		printf( '<section id="big-video" style="background-image: url(%1$s); background-size: cover;">', $poster_url );

		$videos = '';
		foreach( array( 'mp4', 'webm', 'ogv') as $video_type ){

			$video_id = get_theme_mod( $this->setting_prefix . 'video_hero_' . $video_type, 0 );
			$video_url = '';
			if( $video_id ) {
				$video_url = wp_get_attachment_url( $video_id );
				$videos .= sprintf( '<source src="%1$s" type="video/%2$s" />', esc_attr($video_url), $video_type );
			}

		}

		if( ! empty( $videos ) ) {
			echo '<video autoplay loop muted>';
			echo $videos;
			echo '</video>';
		}

		echo '</section>';

	}
}
Streamline_Video_Embed::get_instance();
register_activation_hook(   __FILE__, array( 'Streamline_Video_Embed', 'activate'   ) );
register_deactivation_hook( __FILE__, array( 'Streamline_Video_Embed', 'deactivate' ) );
register_uninstall_hook(    __FILE__, array( 'Streamline_Video_Embed', 'uninstall' ) );
